/* eslint-disable no-useless-concat */
import http from '../common/http/index';
import AppConsts from '../../app-consts';

const apiEndpoint = AppConsts.remoteServiceBaseUrl + "task";

export async function add(add) {
    const data = await http.post('task', add);
    return data;
}
export async function update(update) {
    const data = await http.put('task/'+ `${update.id}`, update);
    return data;
}
export async function deleteTask(_id) {
    const data = await http.delete('task/' + `${_id}`);
    return data;
}
export async function showAll() {
    const data = await http.get('task');
    return data;
}
export async function showAllUnCompleted() {
    const data = await http.get('task?completed=false');
    return data;
}
export async function showAllCompleted() {
    const data = await http.get('task?completed=true');
    return data;
}
export async function showById(id) {
    const data = await http.get('task' + `${id}`);
    return data;
}