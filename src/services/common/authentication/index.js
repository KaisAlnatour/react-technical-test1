
import http from "../http/index";
import AppConsts from "../../../app-consts";

const apiEndpoint = AppConsts.remoteServiceBaseUrl + "user";
const tokenKey = "token";


export async function login(email, password) {
    const data = await http.post(apiEndpoint+'/login', { email, password });
    localStorage.setItem(tokenKey, data.data.token);
    return data;
}

export async function register(user) {
    const data = await http.post(apiEndpoint+'/register', user);
    localStorage.setItem(tokenKey, data.data.token);
    return data;
}

export function loginWithJwt(jwt) {
    localStorage.setItem(tokenKey, jwt);
}

export function logout() {
    localStorage.removeItem(tokenKey);
}

export function getCurrentUser() {
    try {
        const jwt = localStorage.getItem(tokenKey);
        return jwt;
    } catch (ex) {
        return null;
    }
}

export function getJwt() {
    return localStorage.getItem(tokenKey);
}

