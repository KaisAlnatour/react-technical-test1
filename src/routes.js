
import Todo from "views/todo/todo.js";
import Completed from "views/todo/completed";
import LoginPage from "views/log-in";
import RegisterStudent from "views/sign-up";
var routes = [
  {
    exact: true,
    path: "/todo",
    name: "todo",
    rtlName: "",
    icon: "tim-icons icon-bank",
    component: Todo,
    layout: "/admin",
  },
  {
    exact: true,
    path: "/completed",
    name: "completed",
    rtlName: "",
    icon: "tim-icons icon-bank",
    component: Completed,
    layout: "/admin",
  },
  {
    path: "/login",
    name: "",
    component: LoginPage,
    layout: "/user",
  },
  {
    path: "/signup",
    name: "",
    component: RegisterStudent,
    layout: "/user",
  },
];

export default routes;
