import React, { useEffect, useState } from "react";

import { Table, Row, Modal, Button, Form, Col, Spin, Tooltip, Typography, Card } from 'antd';
import { PlusOutlined, DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { columns } from './columns';
import * as todoServices from '../../services/todo/index';
import AddTodoModal from "./add-modal";

function Completed(props) {
    const [isModalVisible, setModalVisible] = useState(false);
    const [isDeleteModalVisible, setDeleteModalVisible] = useState(false);
    const [spinning, setSpinning] = useState(true);
    const [record, setRecord] = useState();
    const [tasks, setTasks] = useState([]);
    const [form] = Form.useForm();
    const [isUpdate, setIsUpdate] = useState(false);


    useEffect(() => {
        getData();
    }, []);

    useEffect(() => {
        if (record) {

            form.setFieldsValue({
                description: record.description,
                completed: record.completed,
            });
        }

    }, [record, form]);
    const getData = () => {
        setSpinning(true);
        (async () => {
            const data = await todoServices.showAllCompleted();
            setTasks(data.data.data);
            setSpinning(false);
        })();
    };
    const handleDelete = () => {
        (async () => {
            const data = await todoServices.deleteTask(record._id);
            getData();
            setDeleteModalVisible(false);
            setSpinning(false);
        })();
    };
    const onFinish = (values) => {
        (async () => {
            await todoServices.add(values);
            setModalVisible(false);
            getData();

        })();
    };

    const handleUpdate = (values) => {
        (async () => {
            await todoServices.update({ ...values, id: record._id });
            setModalVisible(false);
            getData();

        })();
    };

    const actionColumn = {
        key: 'actions',
        width: '13%',
        className: 'actions',
        render: (text, record, index) => {
            return (
                <Row justify="space-between">
                    <Col>
                        <Tooltip title={'Delete'}>
                            <Button
                                // type='link'
                                danger
                                size="small"
                                shape="circle"
                                onClick={() => {
                                    setDeleteModalVisible(true);
                                    setRecord(record);
                                }}
                            >
                                <DeleteOutlined />
                            </Button>
                        </Tooltip>
                    </Col>
                    <Col>
                        <Tooltip title={'Edit'}>
                            <Button
                                type='link'
                                size="small"
                                shape="circle"
                                onClick={() => {
                                    setIsUpdate(true);
                                    setRecord(record);
                                    setModalVisible(true);
                                }}
                            >
                                <EditOutlined />
                            </Button>
                        </Tooltip>
                    </Col>

                </Row>
            );
        },
    };


    return (
        <>
            <div className="content">
                <Card style={{ minHeight: '85vh', borderRadius: '5px' }}>
                    <Spin spinning={spinning} >
                        <Row justify='end' align='middle'>
                            <Button type='primary' onClick={() => { setModalVisible(true); setIsUpdate(false); }} >
                                <Row align='middle'>
                                    <PlusOutlined /> Add 
                                </Row>
                            </Button>
                        </Row>
                        <Row>
                            <Table dataSource={tasks} columns={[...columns, actionColumn]} style={{
                                width: '100%',
                                padding: ' 16px 0 0',
                                borderRadius: '7px'
                            }} />
                        </Row>
                        <AddTodoModal
                            isVisible={isModalVisible}
                            setVisible={setModalVisible}
                            addCourse={onFinish}
                            formValues={record}
                            handleUpdate={handleUpdate}
                            isUpdate={isUpdate}
                        />
                        <Modal
                            title='Delete'
                            visible={isDeleteModalVisible}
                            onCancel={() => { setDeleteModalVisible(false); }}
                            onOk={() => handleDelete()}

                        >
                            <Typography.Text strong>
                                Are you Sure You Want To Delete This Task ?
                            </Typography.Text>

                        </Modal>
                    </Spin>
                </Card>
            </div>
        </>
    );
}

export default Completed;
