import React, { useState, useEffect } from 'react';
import { Row, Modal, Button, Form, Input, Col, InputNumber, Checkbox } from 'antd';

const AddTodoModal = ({ isVisible, setVisible, addCourse, formValues, handleUpdate, isUpdate }) => {
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();

    const onFinish = (values) => {
        const data = values;
        setLoading(true);
        if (isUpdate) {
            (async () => {
                await handleUpdate(data);
                setLoading(false);
            })();
        } else {
            (async () => {
                await addCourse(data);
                setLoading(false);
            })();
        }
        form.resetFields();
    };
    useEffect(() => {
        if (formValues) {
            form.setFieldsValue({
                description: formValues.description,
                completed: formValues.completed,
            });
        }

    }, [formValues, form]);

    return (
        <>
            <Modal
                title={isUpdate ? 'Update' : 'Add'}

                visible={isVisible}
                onCancel={() => { setVisible(false); form.resetFields(); }}
                okButtonProps={{ hidden: true }}
                cancelButtonProps={{ hidden: true }}
                width={675}
            >
                <Form
                    form={form}
                    layout='vertical'
                    onFinish={onFinish}
                >
                    <Row gutter={24} justify='space-between'>
                        <Col sm={24} lg={12}>
                            <Form.Item
                                label="Description"
                                name="description"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input todo description!',
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col sm={24} lg={12}>

                            <Form.Item
                                label="Completed"
                                name="completed"
                                valuePropName="checked"
                            >
                                <Checkbox />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row justify='end'>
                        <Col style={{ margin: '0 8px 0 0' }}>
                            <Form.Item >
                                <Button htmlType="button" onClick={() => {
                                    setVisible(false);
                                    form.resetFields();
                                }}>
                                    Close
                                </Button>
                            </Form.Item>
                        </Col>
                        <Form.Item>
                            <Col>
                                <Button loading={loading} disabled={loading} type="primary" htmlType="submit">
                                    {isUpdate ? 'Update' : 'Add'}
                                </Button>
                            </Col>
                        </Form.Item>
                    </Row>
                </Form>
            </Modal>
        </>
    );

};

export default AddTodoModal;