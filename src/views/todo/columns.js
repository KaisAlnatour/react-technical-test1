import { Row } from "antd";
import { AiOutlineCheck, AiOutlineClose } from 'react-icons/ai';

export const columns = [

    {
        dataIndex: 'description',
        title: 'Description',
    },

    {
        dataIndex: 'completed',
        title: 'Completed',
        render: (text, record) => {
            return (
                record.completed ?
                    <Row>
                        <AiOutlineCheck color='green' fontSize='20px' />
                    </Row>
                    :
                    <Row>
                        <AiOutlineClose color='red' fontSize='20px' />
                    </Row>
            );

        },
    },

];
